
NAME := fdf
CC ?= gcc

MAKE := $(MAKE) --no-print-directory

OBJDIR := build
SRCDIR := src
HEADIR := includes

SRCS := $(shell find $(SRCDIR) -type f -name "*.c")
OBJS := $(SRCS:%=$(OBJDIR)/%.o)
DEPS := $(SRCS:%=$(OBJDIR)/%.d)

HEAD := $(shell find $(SRCDIR) -name "*.h" -and ! -name "*_priv.h")
HEAD := $(subst $(SRCDIR),$(HEADIR),$(HEAD))

LIBS :=	libft/libft.a

#
## Configure OS specific flags

UNAME_S := $(shell uname -s)
ifeq ($(UNAME_S),Darwin)
	LIBS := $(LIBS) minilibx_macos/libmlx.a
	CFLAGS := -Wall -Werror -Wextra -Ofast
	OSFLAGS := -L minilibx_macos -lmlx -framework OpenGl -framework AppKit -lm
else
	LIBS := $(LIBS) minilibx/libminilibx.a
	CFLAGS := -Wall -Werror -Wextra -Ofast -lGL -lX11 -lXext
	OSFLAGS := -lm
endif

# CFLAGS += -DDEBUG

LIBINCS := $(foreach lib,$(LIBS),-I$(dir $(lib))includes)

# This might not be necessary
# _INC := $(shell find $(SRCDIR) -type d)
# INCS := $(addprefix -I,$(_INC))

.PHONY: all re clean fclean debug $(LIBS) _$(NAME)

all: _$(NAME)

_$(NAME): $(LIBS)
	@$(MAKE) $(NAME)

$(NAME): $(HEAD) $(OBJS)
	$(CC) $(CFLAGS) -o $(NAME) $(OBJS) $(LIBS) $(LIBINCS) $(OSFLAGS)

$(OBJDIR) $(HEADIR):
	@mkdir -p $@

-include $(DEPS)

$(OBJDIR)/%.c.o: %.c Makefile
	@mkdir -p $(dir $@)
	$(CC) $(CFLAGS) -MMD -MP -c $< -o $@ $(LIBINCS)

$(HEADIR)/%.h:
	@mkdir -p $(dir $@)
	cp $(subst $(HEADIR),$(SRCDIR),$@) $@

$(LIBS):
	@$(MAKE) -C $(dir $@) $(MAKECMDGOALS)

clean: $(LIBS)
clean:
	rm -f $(OBJS)
	rm -f $(DEPS)
	rm -rf $(OBJDIR)

fclean: $(LIBS)
fclean: clean
	rm -f $(NAME)
	rm -rf $(HEADIR)

re: fclean
	@$(MAKE) all

debug: fclean
	@$(MAKE) all CFLAGS="$(CFLAGS) -O0 -g"

ci:
	@echo "Ci not available"
