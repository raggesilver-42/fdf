/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   aux.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pqueiroz <pqueiroz@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/23 17:10:02 by pqueiroz          #+#    #+#             */
/*   Updated: 2019/10/02 12:32:14 by pqueiroz         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "mlx.h"
#include "fdf.h"
#include "ft_printf/ft_printf.h"
#include <math.h>

void				ft_die(int exit_code, const char *msg, ...)
{
	va_list			ap;

	va_start(ap, msg);
	ft_vdprintf(2, msg, ap);
	va_end(ap);
	exit(exit_code);
}

char				*point_to_string(t_point p)
{
	t_string	*s;
	char		*res;

	s = ft_sprintf("point(x: %.2lf, y: %.2lf, z: %.2lf)", p.x, p.y, p.z);
	res = ft_autofree(s->data);
	free(s);
	return (res);
}

void				fdf_reset(t_mlx *ctx)
{
	if (!(ctx->flags & FDF_CUSTOM_COLOR_SET))
		ctx->basecolor = 0x213D58;
	ctx->color = ctx->basecolor;
	ctx->zoom = WIN_WIDTH / ctx->map->length * 0.25;
	ctx->angle = (t_point){ .x = cos(0.523599), .y = sin(0.523599) };
	ctx->camera = (t_point){ 0, 0, 0 };
	ctx->z_multiplier = 2.0;
	ctx->flags |= FDF_PROJECTION_TYPE_ISO;
}

void				fdf_init(t_mlx *ctx, int argc, const char **argv)
{
	ft_bzero(ctx, sizeof(*ctx));
	if (argc == 2)
		ctx->map = read_map(argv[1]);
	else
		process_args(argv, ctx);
	if (!ctx->map)
		ft_die(1, "Could not read '%s'.\n", argv[argc - 1]);
	ctx->mlx = mlx_init();
	ctx->window = mlx_new_window(ctx->mlx, WIN_WIDTH, WIN_HEIGHT, "Fdf");
	fdf_reset(ctx);
}
