/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pqueiroz <pqueiroz@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/17 15:30:39 by pqueiroz          #+#    #+#             */
/*   Updated: 2019/10/01 20:45:16 by pqueiroz         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"
#include "mlx.h"
#include "ft_printf/ft_printf.h"
#include <math.h>

void		ft_mlx_draw_line(t_mlx *ctx, t_point start, t_point end)
{
	t_point	p;
	t_point	d;
	double	points;

	p = (t_point){ .x = start.x, .y = start.y, .z = 0 };
	d = (t_point){ .x = end.x - start.x, .y = end.y - start.y, .z = 0 };
	points = sqrt(pow(d.x, 2) + pow(d.y, 2));
	d.x /= points;
	d.y /= points;
	while (points > 0)
	{
		mlx_pixel_put(ctx->mlx, ctx->window, p.x, p.y, ctx->color);
		p.x += d.x;
		p.y += d.y;
		points--;
	}
}

/*
** WARN: this function blindingly calls map->data[y]->data[x + 1] without
** verifying any index (make sure y, x, and x + 1 are in range)
*/

void		draw_x(t_mlx *ctx, size_t x, size_t y)
{
	t_point			pos;
	t_point			start;
	t_point			end;
	t_int_arrayt	*line;

	line = (void *)ctx->map->data[y];
	pos.x = (double)((long)x) - line->length / 2;
	pos.y = (double)((long)y) - ctx->map->length / 2;
	start = (t_point){ .x = (ctx->angle.x * (pos.x - pos.y) * ctx->zoom) +
		(WIN_WIDTH / 2) + ctx->camera.x,
		.y = (ctx->angle.y * (pos.x + pos.y) * ctx->zoom) - (line->data[x] *
		ctx->z_multiplier) + (WIN_HEIGHT / 2) + ctx->camera.y,
		.z = line->data[x] };
	end = (t_point){ .x = (ctx->angle.x * (pos.x + 1 - pos.y) * ctx->zoom) +
		(WIN_WIDTH / 2) + ctx->camera.x,
		.y = (ctx->angle.y * (pos.x + 1 + pos.y) * ctx->zoom) - (line->data[x +
		1] * ctx->z_multiplier) + (WIN_HEIGHT / 2) + ctx->camera.y,
		.z = line->data[x + 1] };
	RETURN_IF((start.y < 0 && end.y < 0) || (start.y > WIN_HEIGHT && end.y >
		WIN_HEIGHT) || (start.x < 0 && end.x < 0) || (start.x > WIN_WIDTH &&
		end.x > WIN_WIDTH));
	ft_mlx_draw_line(ctx, start, end);
}

/*
** WARN: this function blindingly calls map->data[y + 1]->data[x] without
** verifying any index (make sure y, x, and y + 1 are in range)
*/

void		draw_y(t_mlx *ctx, size_t x, size_t y)
{
	t_point			pos;
	t_point			start;
	t_point			end;
	t_int_arrayt	*line;
	t_int_arrayt	*line2;

	line = (void *)ctx->map->data[y];
	line2 = (void *)ctx->map->data[y + 1];
	pos.x = (double)((long)x) - line->length / 2;
	pos.y = (double)((long)y) - ctx->map->length / 2;
	start = (t_point){ .x = (ctx->angle.x * (pos.x - pos.y) * ctx->zoom) +
		(WIN_WIDTH / 2) + ctx->camera.x,
		.y = (ctx->angle.y * (pos.x + pos.y) * ctx->zoom) - (line->data[x] *
		ctx->z_multiplier) + (WIN_HEIGHT / 2) + ctx->camera.y,
		.z = line->data[x] };
	end = (t_point){ .x = (ctx->angle.x * (pos.x - pos.y - 1) * ctx->zoom) +
		(WIN_WIDTH / 2) + ctx->camera.x,
		.y = (ctx->angle.y * (pos.x + pos.y + 1) * ctx->zoom) - (line2->data[x]
		* ctx->z_multiplier) + (WIN_HEIGHT / 2) + ctx->camera.y,
		.z = line2->data[x] };
	RETURN_IF((start.y < 0 && end.y < 0) || (start.y > WIN_HEIGHT && end.y >
		WIN_HEIGHT) || (start.x < 0 && end.x < 0) || (start.x > WIN_WIDTH &&
		end.x > WIN_WIDTH));
	ft_mlx_draw_line(ctx, start, end);
}
