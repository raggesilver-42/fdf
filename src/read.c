/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pqueiroz <pqueiroz@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/25 22:32:05 by pqueiroz          #+#    #+#             */
/*   Updated: 2019/10/02 12:05:20 by pqueiroz         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"
#include "libft.h"
#include "ft_printf/ft_printf.h"

/*
** Free the map array ( ArrayT<Array<int>> ).
**
** Will be called from autodestroy.
*/

static void			fk_finalize_map(void **vmap)
{
	size_t			i;
	t_ptr_arrayt	*arr;

	i = 0;
	arr = *vmap;
	while (i < arr->length)
		ARRAYT_DESTROY((t_int_arrayt *)arr->data[i++]);
	ARRAYT_DESTROY(arr);
}

/*
** Free a NULL terminated string array (["aaa", "bbb", NULL])
*/

static void			fk_finalize_str_arr(void **str_arr)
{
	char			**arr;

	arr = *str_arr;
	while (*arr)
		ft_strdel(arr++);
	free(*str_arr);
}

/*
** Modified version of ft_atoi that changes the position of the original string
** as it advances to get the int value.
*/

static int			fk_atoi_walk(const char **str)
{
	char		*p;
	t_int8		mult;
	t_ullong	res;

	mult = 0;
	res = 0;
	p = *(char **)str;
	while (IS_WHITESPACE(*p))
		p++;
	if (*p == '-' && (p++ || 1))
		mult = -1;
	else if ((*p == '+' && (p++ || 1)) || 1)
		mult = 1;
	while (*p >= '0' && *p <= '9' && res <= LONG_LONG_MAX)
		res = (res * 10) + (*p++ - '0');
	*str = p;
	if (res > LONG_LONG_MAX)
		return ((mult == -1) ? 0 : -1);
	return (res * mult);
}

/*
** Validate a line and push it's values to the map array.
*/

static int			fk_process_line(const char *str, t_int_arrayt *arr)
{
	const char		*s;

	s = str;
	while (*s)
	{
		if (!ft_strchr("-0123456789,xABCDEFabcdef \n", *s))
			return (0);
		s++;
	}
	while (*str)
	{
		while (IS_WHITESPACE(*str))
			str++;
		if (*str)
			ARRAYT_PUSH(arr, fk_atoi_walk((const char **)&str));
		while (*str && !(IS_WHITESPACE(*str)))
			str++;
	}
	return (1);
}

/*
** Read the map from path. Allocates and schedules the map destruction.
*/

t_ptr_arrayt		*read_map(const char *path)
{
	t_file			f;
	char			**tmp;
	t_int_arrayt	*line;
	t_ptr_arrayt	*res;
	t_string		*str;

	f = ft_fopen(path, O_RDONLY);
	RETURN_VAL_IF(NULL, (f.fd < 0));
	ARRAYT_INIT(res);
	str = ft_autodestroy(ft_fread(f), (void (*)(void **))&ft_string_destroy);
	tmp = ft_autodestroy(ft_strsplit(str->data, '\n'), &fk_finalize_str_arr);
	ft_autodestroy(res, &fk_finalize_map);
	while (*tmp)
	{
		ARRAYT_INIT(line);
		ARRAYT_PUSH(res, line);
		if ((!fk_process_line(*tmp, line)) || (res->length > 2 &&
			((t_int_arrayt *)res->data[res->length - 1])->length !=
			((t_int_arrayt *)res->data[res->length - 2])->length))
			ft_die(1, "Invalid map.\n");
		tmp++;
	}
	return (res);
}
