/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   args.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pqueiroz <pqueiroz@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/01 21:06:32 by pqueiroz          #+#    #+#             */
/*   Updated: 2019/10/01 21:45:43 by pqueiroz         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

/*
** Returns 1 in case the string is valid, 0 otherwise.
*/

int		fk_string_validate(t_string *self, const char *allowed_chars)
{
	size_t	i;

	i = 0;
	while (i < self->length)
	{
		if (!(ft_strchr(allowed_chars, self->data[i])))
			return (0);
		i++;
	}
	return (1);
}

void	process_args(const char **argv, t_mlx *ctx)
{
	t_string	*color;

	if (ft_strcmp("--color", argv[1]) == 0)
	{
		color = ft_autodestroy(
			ft_string_new(argv[2]), (void (*)(void **))&ft_string_destroy);
		ft_string_to_upper(color);
		if (color->length != 6 || !fk_string_validate(color, g_chars))
			ft_die(1, "Invalid color '%s'\n", argv[2]);
		ctx->basecolor = ft_atoi_base(color->data, 16);
		if (ctx->basecolor < 0 || ctx->basecolor > 0xFFFFFF)
			ft_die(1, "Invalid color '%s'\n", argv[2]);
		ctx->flags |= FDF_CUSTOM_COLOR_SET;
		ctx->map = read_map(argv[3]);
	}
	else
		ft_die(1, "Invalid argument '%s'\n", argv[1]);
}
