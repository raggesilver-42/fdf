/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pqueiroz <pqueiroz@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/12 01:22:44 by pqueiroz          #+#    #+#             */
/*   Updated: 2019/10/02 11:40:08 by pqueiroz         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "mlx.h"
#include "ft_printf/ft_printf.h"
#include "libft.h"
#include "arrayt/arrayt.h"
#include "fdf.h"

#include <stdlib.h>
#include <math.h>

#ifdef DEBUG
# define LOG_KEY	({ ft_printf("[info] Keypress %#06x\n", key); return (0); })
#else
# define LOG_KEY	({ return (0); })
#endif

void			show_commands(void)
{
	ft_printf(
		"pqueiroz's fdf\n\n"
		"%18s: Move the camera\n"
		"%18s: Zoom in/out\n"
		"%18s: Increase/decrease Z multiplier\n"
		"%18s: Move angle (if possible)\n"
		"%18s: Change projection type\n"
		"%18s: Exit\n"
		"\n",
		"UP/DOWN/LEFT/RIGHT", "+/-", "Z/X", "W/S", "P", "Esc");
}

void			draw_map(t_mlx *ctx)
{
	size_t		y;
	size_t		x;

	mlx_clear_window(ctx->mlx, ctx->window);
	y = 0;
	while (y < ctx->map->length)
	{
		x = 0;
		while (x < ((t_int_arrayt *)ctx->map->data[y])->length)
		{
			if (x + 1 < ((t_int_arrayt *)ctx->map->data[y])->length)
				draw_x(ctx, x, y);
			if (y + 1 < ctx->map->length)
				draw_y(ctx, x, y);
			x++;
		}
		y++;
	}
}

void			change_projection(t_mlx *ctx, int key)
{
	if (key > -1 && !(ctx->flags & FDF_PROJECTION_TYPE_ISO))
	{
		if (key == KEY_W || key == KEY_S)
			ctx->angle.y =
				sin(asin(ctx->angle.y) + ((key == KEY_W) ? 0.1 : -0.1));
	}
	else if (key == -1)
	{
		if (ctx->flags & FDF_PROJECTION_TYPE_ISO)
		{
			ctx->angle.y = sin(0.523599) * 0.2;
			ctx->flags &= ~FDF_PROJECTION_TYPE_ISO;
		}
		else
		{
			ctx->angle = (t_point){ .x = cos(0.523599), .y = sin(0.523599) };
			ctx->flags |= FDF_PROJECTION_TYPE_ISO;
		}
	}
}

int				on_key_press(int key, t_mlx *ctx)
{
	if (key == KEY_ESCAPE)
		exit(0);
	else if (key == KEY_LEFT)
		ctx->camera.x += 10;
	else if (key == KEY_RIGHT)
		ctx->camera.x -= 10;
	else if (key == KEY_UP)
		ctx->camera.y += 10;
	else if (key == KEY_DOWN)
		ctx->camera.y -= 10;
	else if (key == KEY_MINUS)
		ctx->zoom = MAX(1, (ctx->zoom * 0.9));
	else if (key == KEY_PLUS)
		ctx->zoom = ceil(ctx->zoom * 1.1);
	else if (key == KEY_Z)
		ctx->z_multiplier -= 0.1;
	else if (key == KEY_X)
		ctx->z_multiplier += 0.1;
	else if (key == KEY_P || key == KEY_W || key == KEY_S)
		change_projection(ctx, (key == KEY_P) ? -1 : key);
	else
		LOG_KEY;
	draw_map(ctx);
	return (0);
}

int				main(int argc, const char **argv)
{
	t_mlx			ctx;

	if (argc != 2 && argc != 4)
		ft_die(1, "Invalid number of arguments.\n");
	fdf_init(&ctx, argc, argv);
	show_commands();
	mlx_hook(ctx.window, EVENT_KEY_PRESS, MASK_NONE, on_key_press, &ctx);
	draw_map(&ctx);
	mlx_loop(ctx.mlx);
	return (0);
}
