/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   gc.c                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pqueiroz <pqueiroz@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/22 22:56:40 by pqueiroz          #+#    #+#             */
/*   Updated: 2019/10/01 15:52:05 by pqueiroz         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "arrayt/arrayt.h"
#include "gc_priv.h"
#include "fdf.h"

#define TO_FREE				0
#define TO_DESTROY			1
#define MAYBE_TO_FREE		2
#define MAYBE_TO_DESTROY	3

static void					*fk_get(int i)
{
	static t_dtrinfo_arrayt	*to_destroy = NULL;
	static t_ptr_arrayt		*to_free = NULL;

	if (i == MAYBE_TO_DESTROY)
		return (to_destroy);
	if (i == MAYBE_TO_FREE)
		return (to_free);
	if (i == TO_FREE)
	{
		if (to_free == NULL)
			ARRAYT_INIT(to_free);
		return (to_free);
	}
	else
	{
		if (to_destroy == NULL)
			ARRAYT_INIT(to_destroy);
		return (to_destroy);
	}
}

void						*ft_autofree(void *ptr)
{
	t_ptr_arrayt	*to_free;

	to_free = fk_get(TO_FREE);
	ARRAYT_PUSH(to_free, ptr);
	return (ptr);
}

void						*ft_autodestroy(void *ptr, t_destroy_function *fn)
{
	t_destroy_info		*info;
	t_dtrinfo_arrayt	*to_destroy;

	to_destroy = fk_get(TO_DESTROY);
	info = malloc(sizeof(*info));
	info->ptr = ptr;
	info->fn = fn;
	ARRAYT_PUSH(to_destroy, info);
	return (ptr);
}

/*
** void						fkgc_destructor(void) __attribute__((destructor))
*/

void						fkgc_destructor(void)
{
	size_t				i;
	t_ptr_arrayt		*to_free;
	t_dtrinfo_arrayt	*to_destroy;

	to_free = fk_get(MAYBE_TO_FREE);
	to_destroy = fk_get(MAYBE_TO_DESTROY);
	if (to_free)
	{
		i = 0;
		while (i < to_free->length)
			free(to_free->data[i++]);
		ARRAYT_DESTROY(to_free);
	}
	if (to_destroy)
	{
		i = 0;
		while (i < to_destroy->length)
		{
			((t_destroy_function *)to_destroy->data[i]->fn)(
				&to_destroy->data[i]->ptr);
			i++;
		}
		arrayt_destroy_with_func(to_destroy, &ft_memdel);
	}
}
