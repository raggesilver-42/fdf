/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pqueiroz <pqueiroz@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/22 18:16:32 by pqueiroz          #+#    #+#             */
/*   Updated: 2019/10/02 10:16:55 by pqueiroz         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FDF_H
# define FDF_H

# include "gc.h"
# include "arrayt/arrayt.h"

# define WIN_WIDTH	1200
# define WIN_HEIGHT	675

# ifdef __linux__
#  define KEY_ESCAPE 	0xff1b
# else
#  define KEY_ESCAPE	0x35
#  define KEY_LEFT		0x7b
#  define KEY_RIGHT		0x7c
#  define KEY_DOWN		0x7d
#  define KEY_UP		0x7e
#  define KEY_MINUS		0x1b
#  define KEY_PLUS		0x18
#  define KEY_Z			0x06
#  define KEY_X			0x07
#  define KEY_P			0x23
#  define KEY_W			0x0d
#  define KEY_A			0x00
#  define KEY_S			0x01
#  define KEY_D			0x02
# endif

# define EVENT_KEY_PRESS	2
# define MASK_NONE			0

enum				e_fdf_flags
{
	FDF_GRADIENT_ON = 1 << 0,
	FDF_PROJECTION_TYPE_ISO = 1 << 1,
	FDF_PROJECTION_TYPE_PARALLEL = 1 << 2,
	FDF_CUSTOM_COLOR_SET = 1 << 3
};

typedef struct		s_int_arrayt
{
	int				*data;
	size_t			length;
	size_t			size;
}					t_int_arrayt;

typedef struct		s_ptr_arrayt
{
	void			**data;
	size_t			length;
	size_t			size;
}					t_ptr_arrayt;

typedef struct		s_point
{
	double			x;
	double			y;
	double			z;
}					t_point;

typedef struct		s_mlxctx
{
	void			*mlx;
	void			*window;
	double			z_multiplier;
	t_point			angle;
	t_point			camera;
	int				zoom;
	int				basecolor;
	int				color;
	int				end_color;
	int				flags;
	t_ptr_arrayt	*map;
}					t_mlx;

t_ptr_arrayt		*read_map(const char *path);
void				ft_die(int exit_code, const char *msg,
						...) __attribute__((format(printf,2,3)));

char				*point_to_string(t_point p);
void				draw_x(t_mlx *ctx, size_t x, size_t y);
void				draw_y(t_mlx *ctx, size_t x, size_t y);
void				ft_mlx_draw_line(t_mlx *ctx, t_point start, t_point end);

void				fdf_init(t_mlx *ctx, int argc, const char **argv);

void				process_args(const char **argv, t_mlx *ctx);

#endif
