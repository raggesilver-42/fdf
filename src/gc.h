/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   gc.h                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pqueiroz <pqueiroz@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/22 22:56:40 by pqueiroz          #+#    #+#             */
/*   Updated: 2019/09/25 23:20:19 by pqueiroz         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GC_H
# define GC_H

typedef void		(t_destroy_function)(void **ptr);

typedef struct		s_destroy_info
{
	void			*ptr;
	void			*fn;
}					t_destroy_info;

void				*ft_autofree(void *ptr);
void				*ft_autodestroy(void *ptr, t_destroy_function *fn);

#endif
