/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   gc_priv.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pqueiroz <pqueiroz@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/22 22:56:40 by pqueiroz          #+#    #+#             */
/*   Updated: 2019/09/27 00:00:25 by pqueiroz         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GC_PRIV_H
# define GC_PRIV_H

# include "gc.h"
# include <stdlib.h>

typedef struct		s_dtrinfo_arrayt
{
	t_destroy_info	**data;
	size_t			length;
	size_t			size;
}					t_dtrinfo_arrayt;

void				fkgc_destructor(void) __attribute__((destructor));

#endif
